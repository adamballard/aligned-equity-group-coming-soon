<?php

    if (!isset($_POST)){
        die('Invalid Form Submission');
    }

    $form_inputs = array(
        'contact-name' => array('label' => 'Name:'),
        'contact-phone' => array('label' => 'Phone Number:'),
        'contact-email' => array('label' => 'Email:'),
        'contact-message' => array('label' => 'Message:')
    );
    
    $input_values = array();

    foreach ($_POST as $key=>$val){
        $key = htmlspecialchars( $key );
        $val = htmlspecialchars( $val );

        $input_values[$key] = $val;
    }

    $recipient = 'ab@timmermanngroup.com';
    $subject = 'Aligned Equity Group Coming Soon Contact Form';
    $message = '';

    foreach ($form_inputs as $key => $label){
        if(key_exists($key, $input_values)){
            $message .= $label['label']. ' ' . $input_values[$key] ;
        }
    }

    mail($recipient, $subject, $message);
    header('index.php');

    // khoffman@aegfunds.com