/* ==========================================================================================
  Theme Name: Bizz
  Author: vs-themes
  Support: visakhinkollam@gmail.com
  Description: Business Consulting and Agency HTML 5 Template
  Version: 1.0
========================================================================================== */


/** ===============

01.Preloader fade out

=============== */
(function($) {
        "use strict";
/*------------------------------------------------------------------------------*/
        /*01.Preloader fade out
/*------------------------------------------------------------------------------*/
        $(window).on('load', function() {
            $('.loader-page').delay('300').fadeOut(1000);
        });
    }

)(jQuery); // End jQuery